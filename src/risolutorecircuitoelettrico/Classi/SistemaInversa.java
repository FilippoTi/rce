package risolutorecircuitoelettrico.Classi;

public class SistemaInversa extends MatriceQuadrata implements SistemaLineare
{
    /**
     * Costruttore
     * @param ordine l'ordine della matrice incompleta 
     */
    public SistemaInversa(int ordine)
    {
        super(ordine);
    }

    /**
     * Risolve il sistema
     * @param TerminiNoti matrice dei termini noti
     * @return un oggetto Matrice
     */
    @Override
    public Matrice Risolvi(Matrice TerminiNoti)
    {
        MatriceQuadrata A = MatriceQuadrata.MatriceInversa(this);
        
        return Matrice.ProdottoMatrici(A, TerminiNoti);
    }

}

package risolutorecircuitoelettrico;

import risolutorecircuitoelettrico.Classi.*;

public class testClass
{

    public static void Test(String NomeFile, int pos, boolean mA)
    {
        Matrice Gestore = new Matrice();
        if ((Gestore.LetturaDaFile(NomeFile, true)) == 0)
        {
            int i, j;
            Matrice terminiNoti = new Matrice(Gestore.getNumeroDiRighe(), 1);
            SistemaInversa incompleta = new SistemaInversa(Gestore.getNumeroDiRighe());

            for (i = 0; i < terminiNoti.getNumeroDiRighe(); i++)
            {
                terminiNoti.setElemento(i, 0, Gestore.getElemento(i, Gestore.getNumeroDiColonne() - 1));
            }

            for (i = 0; i < incompleta.getNumeroDiRighe(); i++)
            {
                for (j = 0; j < incompleta.getNumeroDiColonne(); j++)
                {
                    incompleta.setElemento(i, j, Gestore.getElemento(i, j));
                }
            }
            if (mA)
            {
                System.out.printf("%.3fmA\n", incompleta.Risolvi(terminiNoti).getElemento(pos, 0) * 1000);
            } else
            {
                System.out.printf("%.3fA\n", incompleta.Risolvi(terminiNoti).getElemento(pos, 0));
            }

        } else
        {
            System.err.println("Impossibile aprire il file");
        }
    }

    public static void main(String[] args)
    {
        Test("circ1.txt", 3, true);
        Test("circ2.txt", 1, false);
    }
}
